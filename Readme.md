# MJML boilerplate

## Install

`npm install`

## Usage

`npm start`

## Build

`npm run build`

## Useful Resources

1. [MJML documentaion](https://documentation.mjml.io/)
2. [MJML Sandbox](https://mjml.io/try-it-live/)
3. [MJML Examples](https://mjml.io/templates)
4. [Sending test emails](https://putsmail.com/)
5. [Litmus](https://www.litmus.com/)
6. [Previous project](https://github.com/BikbaevFR/tp3-new-emails)
